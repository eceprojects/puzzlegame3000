#include <string>
#include "BlocsBoug.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
BlocsBoug::BlocsBoug() : b_posx(13), b_posy(13), b_etat(1), b_effet(0)
{
}

BlocsBoug::BlocsBoug(int x, int y, int etat, int effet) : b_posx(x), b_posy(y), b_etat(etat), b_effet(effet)
{
}

BlocsBoug::~BlocsBoug()
{
}

////////////////////////////////////////Getter et Setter
int BlocsBoug::getPosx() const
{
    return b_posx;
}

int BlocsBoug::getPosy() const
{
    return b_posy;
}

int BlocsBoug::getEtat() const
{
    return b_etat;
}

int BlocsBoug::getEffet() const
{
    return b_effet;
}

void BlocsBoug::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        b_posx = positionx;
}

void BlocsBoug::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        b_posy = positiony;
}

void BlocsBoug::setEtat(int eta)
{
    if (eta==0 || eta==1)
        b_etat = eta;
}

void BlocsBoug::setEffet(int effect)
{
    if(effect==0 || effect==1)
        b_effet = effect;
}


