#include <string>
#include "BlocsCass.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
BlocsCass::BlocsCass() : b_posx(13), b_posy(13), b_etat(1), b_effet(2)
{
}

BlocsCass::BlocsCass(int x, int y, int etat, int effet) : b_posx(x), b_posy(y), b_etat(etat), b_effet(effet)
{
}

BlocsCass::~BlocsCass()
{
}

////////////////////////////////////////Getter et Setter
int BlocsCass::getPosx() const
{
    return b_posx;
}

int BlocsCass::getPosy() const
{
    return b_posy;
}

int BlocsCass::getEtat() const
{
    return b_etat;
}

int BlocsCass::getEffet() const
{
    return b_effet;
}

void BlocsCass::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        b_posx = positionx;
}

void BlocsCass::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        b_posy = positiony;
}

void BlocsCass::setEtat(int eta)
{
    if (eta==0 || eta==1)
        b_etat = eta;
}

void BlocsCass::setEffet(int effect)
{
    if(effect==0 || effect==1)
        b_effet = effect;
}



