#include <string>
#include "BlocsImmo.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
BlocsImmo::BlocsImmo() : b_posx(10), b_posy(10), b_etat(0), b_effet(0)
{
}

BlocsImmo::BlocsImmo(int x, int y, int etat, int effet) : b_posx(x), b_posy(y), b_etat(etat), b_effet(effet)
{
}

BlocsImmo::~BlocsImmo()
{
}

////////////////////////////////////////Getter et Setter
int BlocsImmo::getPosx() const
{
    return b_posx;
}

int BlocsImmo::getPosy() const
{
    return b_posy;
}

int BlocsImmo::getEtat() const
{
    return b_etat;
}

int BlocsImmo::getEffet() const
{
    return b_effet;
}

void BlocsImmo::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        b_posx = positionx;
}

void BlocsImmo::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        b_posy = positiony;
}

void BlocsImmo::setEtat(int eta)
{
    if (eta==0 || eta==1)
        b_etat = eta;
}

void BlocsImmo::setEffet(int effect)
{
    if(effect==0 || effect==1)
        b_effet = effect;
}

