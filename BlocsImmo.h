#ifndef BLOCSIMMO_H_INCLUDED
#define BLOCSIMMO_H_INCLUDED

#include <string>


class BlocsImmo
{

private:
    int b_posx, b_posy; //la position
    int b_etat; //l'etat mort ou vivant (existe ou non)
    int b_effet; //le bloc tue ou autre 0->rien 1->tue...

public:
    //Constructeurs et destructeur
    BlocsImmo();
    BlocsImmo(int x, int y, int etat, int effet);
    ~BlocsImmo();

    //Methodes
    /*
        Tous les sp li�s � la classe
    */

    //Getter et setter
    int getPosx() const;
    int getPosy() const;
    int getEtat() const;
    int getEffet() const;

    void setPosx(int positionx);
    void setPosy(int positiony);
    void setEtat(int eta);
    void setEffet(int effect);

};


#endif // BLOCS_H INCLUDED


