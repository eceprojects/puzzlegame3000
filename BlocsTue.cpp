#include <string>
#include "BlocsTue.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
BlocsTue::BlocsTue() : b_posx(13), b_posy(13), b_etat(1), b_effet(1)
{
}

BlocsTue::BlocsTue(int x, int y, int etat, int effet) : b_posx(x), b_posy(y), b_etat(etat), b_effet(effet)
{
}

BlocsTue::~BlocsTue()
{
}

////////////////////////////////////////Getter et Setter
int BlocsTue::getPosx() const
{
    return b_posx;
}

int BlocsTue::getPosy() const
{
    return b_posy;
}

int BlocsTue::getEtat() const
{
    return b_etat;
}

int BlocsTue::getEffet() const
{
    return b_effet;
}

void BlocsTue::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        b_posx = positionx;
}

void BlocsTue::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        b_posy = positiony;
}

void BlocsTue::setEtat(int eta)
{
    if (eta==0 || eta==1)
        b_etat = eta;
}

void BlocsTue::setEffet(int effect)
{
    if(effect==0 || effect==1)
        b_effet = effect;
}


