#include "Contrat.h"

Contrat::Contrat() : c_timer(0), c_timerMax(60), c_score(0), c_scoreTotal(0)
{
}

Contrat::Contrat(int timer, int timerMax, int score, int scoretot) : c_timer(timer), c_timerMax(timerMax), c_score(score), c_scoreTotal(scoretot){}

Contrat::~Contrat()
{
}
//////////////////////////////////////

int Contrat::getTimer() const
{
    return c_timer;
}

int Contrat::getTimerMax() const
{
    return c_timerMax;
}

int Contrat::getScore() const
{
    return c_score;
}

int Contrat::getScoreTotal() const
{
    return c_scoreTotal;
}

void Contrat::setTimer(int tps)
{
    if (tps>=0 && tps<=60)
        c_timer = tps;
}

void Contrat::setTimerMax(int tpsmax)
{
    if (tpsmax>=30 && tpsmax<=60)
        c_timerMax = tpsmax;
}

void Contrat::setScore(int scorej)
{
    if (scorej>=0 && scorej<=60*100)
        c_score = scorej;
}

void Contrat::setScoreTotal(int scoret)
{
    c_scoreTotal = scoret;
}

int Contrat::score()
{
    int scorenow;
    scorenow = ((getTimerMax() - getTimer()) *100);
    return scorenow;
}
