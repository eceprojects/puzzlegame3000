#ifndef CONTRAT_H
#define CONTRAT_H


class Contrat
{
    private:
        int c_timer; //le timer
        int c_timerMax; //le tps max du niveau
        int c_score; //le score du joueur
        int c_scoreTotal; //le score du joueur


    public:
        Contrat();
        Contrat(int timer, int timerMax, int score, int scoretot);
        ~Contrat();

        int score();
        /*
            glisser les proto des SP
        */

        int getTimer() const;
        int getTimerMax() const;
        int getScore() const;
        int getScoreTotal() const;

        void setTimer(int tps);
        void setTimerMax(int tpsmax);
        void setScore(int scorej);
        void setScoreTotal(int scoret);


};

#endif // CONTRAT_H

