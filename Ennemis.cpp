#include <string>
#include "Ennemis.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs

Ennemis::Ennemis() : e_posx(5), e_posy(10), e_dx(1), e_dy(1), e_etat(1), e_type(1), e_time(0)
{
}

Ennemis::Ennemis(int x, int y, int depx, int depy, int etat, int type, int time) : e_posx(x), e_posy(y), e_dx(depx), e_dy(depy), e_etat(etat), e_type(type), e_time(time)
{
}

Ennemis::~Ennemis()
{
}

////////////////////////////////////////Getter et Setter
int Ennemis::getPosx() const
{
    return e_posx;
}

int Ennemis::getPosy() const
{
    return e_posy;
}

int Ennemis::getDx() const
{
    return e_dx;
}

int Ennemis::getDy() const
{
    return e_dy;
}

int Ennemis::getEtat() const
{
    return e_etat;
}

int Ennemis::getType() const
{
    return e_type;
}

int Ennemis::getTime() const
{
    return e_time;
}

void Ennemis::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        e_posx = positionx;
}

void Ennemis::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        e_posy = positiony;
}

void Ennemis::setDx(int deplacementx)
{
    if (deplacementx!=0) e_dx = deplacementx;
}

void Ennemis::setDy(int deplacementy)
{
    if (deplacementy!=0) e_dy = deplacementy;
}

void Ennemis::setEtat(int eta)
{
    if (eta==0 || eta==1)
        e_etat = eta;
}

void Ennemis::setType(int typ)
{
    if(typ==1 || typ==2)
        e_type = typ;
}

void Ennemis::setTime(int time)
{
    if(time>=0)
        e_time = time;
}

bool Ennemis::deplacerEnnemi(bool changex)
{
    bool aff=false;
    bool changey=false;
    setTime(getTime()+1);
    //cout<<getTime()<<getDx()<<getDy()<<"  "<<getPosx()<<getPosy();
    if(getTime()==1000 )
    {
        if(changex==false)
        {
            setPosx(getPosx()+ getDx());
            setPosy(getPosy()+ getDy());
        }

        if(getPosx()==9||getPosx()==0||changex==true)
        {
            setDx(getDx()*(-1));
        }
        if(getPosy()==19||getPosy()==0||changey==true)
        {
            setDy(getDy()*(-1));
        }
        setTime(0);
        aff=true;
    }
 return aff;
}
