#ifndef ENNEMIS_H_INCLUDED
#define ENNEMIS_H_INCLUDED
#include <iostream>
#include <string>


class Ennemis
{

private:
    int e_posx, e_posy; //la position
    int e_dx, e_dy; //le mouvement
    int e_etat; //l'etat mort ou vivant
    int e_type; //le type d'ennemis (balle ou autre) 1->la balle 2->ennemis lambda
    int e_time;
public:
    //Constructeurs et destructeur
    Ennemis();
    Ennemis(int x, int y, int depx, int depy, int etat, int type, int time);
    ~Ennemis();

    //Methodes
    bool deplacerEnnemi(bool changex);
    /*
        Tous les sp li�s � la classe
    */

    //Getter et setter
    int getPosx() const;
    int getPosy() const;
    int getDx() const;
    int getDy() const;
    int getEtat() const;
    int getType() const;
    int getTime() const;

    void setPosx(int positionx);
    void setPosy(int positiony);
    void setDx(int deplacementx);
    void setDy(int deplacementy);
    void setEtat(int eta);
    void setType(int typ);
    void setTime(int time);
};


#endif // ENNEMIS_H INCLUDED
