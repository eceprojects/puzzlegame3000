#include "Menu.h"

Menu::Menu()
{
}

Menu::~Menu()
{
    //dtor
}

        bool diplayMenu() // Use this to display the menu you previously created
        {

        }
        bool debugMenu(); // Displays all the infos in the menu (buttons name, number, actions)

        bool addButton(std::string name, std::string action); // Adds one button at the end of the menu, optionally adds an action description
        bool deleteButton(std::string); // Deletes button by name
        bool deleteNumber(short int number); // Deletes button by its number (1...n)
        bool insertButton(short int number, std::string name, std::string action); // Insert a button at a specific rank/number
