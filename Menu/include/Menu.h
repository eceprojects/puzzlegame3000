#ifndef MENU_H
#define MENU_H
#include <iostream>
#include<vector>
#include <string>
#include <algorithm>

class Menu
{
    public:
        Menu();
        ~Menu();

        bool diplayMenu(); // Use this to display the menu you previously created
        bool debugMenu(); // Displays all the infos in the menu (buttons name, number, actions)

        bool addButton(std::string name, int (*action)(int)); // Adds one button at the end of the menu, with a reference of the function
        bool deleteButton(std::string); // Deletes button by name
        bool deleteNumber(short int number); // Deletes button by its number (1...n)
        bool insertButton(short int number, std::string name, std::string action); // Insert a button at a specific rank/number

    private:
        std::string m_log; // A diagnostic/log string

        struct m_button // Struct containing a button basic info
        {
            std::string name;
            int (*action)(int); // Points the function called if the button is selected
            short int num;

        };
        short int m_totalNumber; // Total number of buttons in the menu
        std::vector<m_button> m_buttonList; // The list containing all buttons
};

#endif // MENU_H
