#include <string>
#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include "Niveaux.h"
#include "Objectifs.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

// On utilise un systeme de fichier afin de charger nos niveaus: 0 representant le vide, 1 les objectifs, 2 les blocs normaux, 3 les blocs qui bougent, 4 les blocs cassant, 5 les blocs pi�g�s

//////////////////////////////////////////Constructeurs et destructeurs
Niveaux::Niveaux() : n_num(1), n_numMax(3)
{
    matriceJeu.resize(10);
    for(int i=0;i<10;i++)
    {
        matriceJeu[i].resize(20);
    }
}

Niveaux::Niveaux(int _num, int _numMax) : n_num(_num), n_numMax(_numMax)
{
}

Niveaux::~Niveaux()
{
}

////////////////////////////////////////Getter et Setter
int Niveaux::getNum() const
{
    return n_num;
}

int Niveaux::getNumMax() const
{
    return n_numMax;
}

void Niveaux::setNum(int numlvl)
{
    if (numlvl>=1 && numlvl<=3)
        n_num = numlvl;
}

void Niveaux::setNumMax(int numlvlmax)
{
    if (numlvlmax==3)
        n_num = numlvlmax;
}

void Niveaux::deplacerPersonnage(int key)
{
    switch(key)
    {
    case 90://Z
    case 122:
        if(personnage.getPosx()>=1)
    {
        for(unsigned int i=0; i<tabObj.size(); i++)
        {
            if (personnage.getPosx()-1 == tabObj[i]->getPosx() && personnage.getPosy() == tabObj[i]->getPosy())
            {
                if(tabObj[i]->getEtat()==1)
                {
                    tabObj[i]->setEtat(0);
                }
            }
        }
        if(matriceJeu[personnage.getPosx()-1][personnage.getPosy()]=='.' || matriceJeu[personnage.getPosx()-1][personnage.getPosy()]=='1')
        {
            personnage.setPosx(personnage.getPosx()-1);
            break;
        }
        for(unsigned int i=0; i<tabBlocsImmo.size(); i++)
        {
            if (personnage.getPosx()-1 == tabBlocsImmo[i]->getPosx() && personnage.getPosy() == tabBlocsImmo[i]->getPosy()) {break;}
        }
        for(unsigned int i=0; i<tabBlocsBoug.size(); i++)
        {
            if (personnage.getPosx()-1 == tabBlocsBoug[i]->getPosx() && personnage.getPosy() == tabBlocsBoug[i]->getPosy() && personnage.getPosx()>=2)
            {
                if(tabBlocsBoug[i]->getEtat()==1)
                {
                    tabBlocsBoug[i]->setPosx(tabBlocsBoug[i]->getPosx()-1);
                    personnage.setPosx(personnage.getPosx()-1);
                    tabBlocsBoug[i]->setEtat(0);
                    break;
                }else break;
            }
        }
        for(unsigned int i=0; i<tabBlocsTue.size(); i++)
        {
            if (personnage.getPosx()-1 == tabBlocsTue[i]->getPosx() && personnage.getPosy() == tabBlocsTue[i]->getPosy())
            {
                personnage.setPosx(personnage.getPosx()-1);
                personnage.setVie(0);
                break;
            }
        }
    }
    break;

    case 83://S
    case 115: if(personnage.getPosx()<=8 )
    {
        for(unsigned int i=0; i<tabObj.size(); i++)
        {
            if (personnage.getPosx()+1 == tabObj[i]->getPosx() && personnage.getPosy() == tabObj[i]->getPosy())
            {
                if(tabObj[i]->getEtat()==1)
                {
                    tabObj[i]->setEtat(0);
                }
                break;
            }
        }
        if(matriceJeu[personnage.getPosx()+1][personnage.getPosy()]=='.'||matriceJeu[personnage.getPosx()+1][personnage.getPosy()]=='1')
        {
            personnage.setPosx(personnage.getPosx()+1);
            break;
        }

        for(unsigned int i=0; i<tabBlocsImmo.size(); i++)
        {
            if (personnage.getPosx()+1 == tabBlocsImmo[i]->getPosx() && personnage.getPosy() == tabBlocsImmo[i]->getPosy()) {break;}
        }
        for(unsigned int i=0; i<tabBlocsBoug.size(); i++)
        {
            if (personnage.getPosx()+1 == tabBlocsBoug[i]->getPosx() && personnage.getPosy() == tabBlocsBoug[i]->getPosy() && personnage.getPosx()<8)
            {
                if(tabBlocsBoug[i]->getEtat()==1)
                {
                    tabBlocsBoug[i]->setPosx(tabBlocsBoug[i]->getPosx()+1);
                    personnage.setPosx(personnage.getPosx()+1);
                    tabBlocsBoug[i]->setEtat(0);
                    break;
                }else break;
            }
        }
        for(unsigned int i=0; i<tabBlocsTue.size(); i++)
        {
            if (personnage.getPosx()+1 == tabBlocsTue[i]->getPosx() && personnage.getPosy() == tabBlocsTue[i]->getPosy())
            {
                personnage.setPosx(personnage.getPosx()+1);
                personnage.setVie(0);
                break;
            }
        }
    }
    break;
    case 81://Q
    case 113: if(personnage.getPosy()>=1)
    {
        for(unsigned int i=0; i<tabObj.size(); i++)
        {
            if (personnage.getPosx() == tabObj[i]->getPosx() && personnage.getPosy()-1 == tabObj[i]->getPosy())
            {
                if(tabObj[i]->getEtat()==1)
                {
                    tabObj[i]->setEtat(0);
                }
                break;
            }
        }
         if(matriceJeu[personnage.getPosx()][personnage.getPosy()-1]=='.'||matriceJeu[personnage.getPosx()][personnage.getPosy()-1]=='1')
         {
             personnage.setPosy(personnage.getPosy()-1);
             break;
         }
        for(unsigned int i=0; i<tabBlocsImmo.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsImmo[i]->getPosx() && personnage.getPosy()-1 == tabBlocsImmo[i]->getPosy()) {break;}
        }
        for(unsigned int i=0; i<tabBlocsBoug.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsBoug[i]->getPosx() && personnage.getPosy()-1 == tabBlocsBoug[i]->getPosy() && personnage.getPosy()>=2)
            {
                if(tabBlocsBoug[i]->getEtat()==1)
                {
                    tabBlocsBoug[i]->setPosy(tabBlocsBoug[i]->getPosy()-1);
                    personnage.setPosy(personnage.getPosy()-1);
                    tabBlocsBoug[i]->setEtat(0);
                    break;
                }else break;
            }
        }
        for(unsigned int i=0; i<tabBlocsTue.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsTue[i]->getPosx() && personnage.getPosy()-1 == tabBlocsTue[i]->getPosy())
            {

                personnage.setPosy(personnage.getPosy()-1);
                personnage.setVie(0);
                break;
            }
        }

    }
    break;
    case 68://D
    case 100: if(personnage.getPosy()<=18)
    {
        for(unsigned int i=0; i<tabObj.size(); i++)
        {
            if (personnage.getPosx() == tabObj[i]->getPosx() && personnage.getPosy()+1 == tabObj[i]->getPosy())
            {
                if(tabObj[i]->getEtat()==1)
                {
                    tabObj[i]->setEtat(0);
                }
                break;
            }
        }

        if(matriceJeu[personnage.getPosx()][personnage.getPosy()+1]=='.'||matriceJeu[personnage.getPosx()][personnage.getPosy()+1]=='1')
        {
            personnage.setPosy(personnage.getPosy()+1);
            break;
        }
        for(unsigned int i=0; i<tabBlocsImmo.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsImmo[i]->getPosx() && personnage.getPosy()+1 == tabBlocsImmo[i]->getPosy()) {break;}
        }
        for(unsigned int i=0; i<tabBlocsBoug.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsBoug[i]->getPosx() && personnage.getPosy()+1 == tabBlocsBoug[i]->getPosy() && personnage.getPosy()<18)
            {
                if(tabBlocsBoug[i]->getEtat()==1)
                {
                    personnage.setPosy(personnage.getPosy() + 1);
                    tabBlocsBoug[i]->setPosy(tabBlocsBoug[i]->getPosy() + 1);
                    tabBlocsBoug[i]->setEtat(0);
                    break;
                }else break;
            }
        }
        for(unsigned int i=0; i<tabBlocsTue.size(); i++)
        {
            if (personnage.getPosx() == tabBlocsTue[i]->getPosx() && personnage.getPosy()+1 == tabBlocsTue[i]->getPosy())
            {

                personnage.setPosy(personnage.getPosy()+1);
                personnage.setVie(0);
                break;
            }
        }

    }
    break;
    case 65://A
    case 97:
        for (unsigned int i=0; i<tabBlocsCass.size();i++)
        {
            if((personnage.getPosx()+1==tabBlocsCass[i]->getPosx() && (personnage.getPosy()==tabBlocsCass[i]->getPosy()))||(personnage.getPosx()-1==tabBlocsCass[i]->getPosx() && (personnage.getPosy()==tabBlocsCass[i]->getPosy()))||(personnage.getPosx()==tabBlocsCass[i]->getPosx() && (personnage.getPosy()-1==tabBlocsCass[i]->getPosy()))||(personnage.getPosx()==tabBlocsCass[i]->getPosx() && (personnage.getPosy()+1==tabBlocsCass[i]->getPosy())))
            {
                tabBlocsCass[i]->setEtat(0);
            }
        }
    //cout<< getPosx()<<"  "<<getPosy()<<endl;
}
}

void Niveaux::afficherScore(time_t temps1, int &scoretmp ,int cmp, int nbvie)
{
        time_t temps2;
        temps2 = time(NULL);
        int score = 0;
        score = contrat.score();
        contrat.setTimer(difftime(temps2,temps1));
        Console* pConsole = NULL;
        pConsole = Console::getInstance();
        int x=30,y=21;
        pConsole->gotoLigCol(y,x);
        cout << "-------------------------------------------------------------";
        pConsole->gotoLigCol(y+1,x);
        cout<<  "                           SCORES                            ";
        pConsole->gotoLigCol(y+2,x+1);
        cout << "-----------------------------------------------------------";
        pConsole->gotoLigCol(y+3,x+20);
        pConsole->setColor(COLOR_JAUNE);
        if(difftime(temps2,temps1)>50)
        {   pConsole->setColor(COLOR_ROUGE);
            cout<< "Temps restant : 0" << 60-difftime(temps2,temps1);
        }else
        {pConsole->setColor(COLOR_JAUNE);cout<< "Temps restant : " << 60-difftime(temps2,temps1);}
        pConsole->setColor(COLOR_WHITE);
        pConsole->gotoLigCol(y+5,x+42);
        if(difftime(temps2,temps1)>50)
        {
            cout << "Score Niveau: 0" << score;
        }else cout << "Score Niveau: " << score;
        pConsole->gotoLigCol(y+7,x+42);
        cout << "Score Total: " << scoretmp;
        pConsole->gotoLigCol(y+7,x+2);
        cout << "Vie(s): " << nbvie <<"/3";
        pConsole->gotoLigCol(y+5,x+2);
        cout << "Oiseau(x): " << cmp <<"/4";
        pConsole->gotoLigCol(y+1,x);
        cout << "|";
        pConsole->gotoLigCol(y+2,x);
        cout << "|";
        pConsole->gotoLigCol(y+3,x);
        cout << "|";
        pConsole->gotoLigCol(y+4,x);
        cout << "|";
        pConsole->gotoLigCol(y+5,x);
        cout << "|";
        pConsole->gotoLigCol(y+6,x);
        cout << "|";
        pConsole->gotoLigCol(y+7,x);
        cout << "|";
        pConsole->gotoLigCol(y+1,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+2,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+3,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+4,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+5,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+6,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+7,x+60);
        cout << "|";
        pConsole->gotoLigCol(y+8,x);
        cout << "-------------------------------------------------------------";

}

void Niveaux::afficherNiveau()
{
    pConsole = Console::getInstance();
    for(int i=0; i<10; i++)
    {
        for (int j=0; j<20; j++)
        {
            matriceJeu[i][j]='.';
        }
    }

    for(unsigned int i=0;i<tabBlocsImmo.size();i++)
    {
            matriceJeu[tabBlocsImmo[i]->getPosx()][tabBlocsImmo[i]->getPosy()]='3';
    }
    for(unsigned int i=0;i<tabObj.size();i++)
    {
        if(tabObj[i]->getEtat()==1)
        {
            matriceJeu[tabObj[i]->getPosx()][tabObj[i]->getPosy()]='1';
        }else matriceJeu[tabObj[i]->getPosx()][tabObj[i]->getPosy()]='.';

    }
    for(unsigned int i=0;i<tabBlocsBoug.size();i++)
    {
        if(tabBlocsBoug[i]->getEtat()==1)
        {
            matriceJeu[tabBlocsBoug[i]->getPosx()][tabBlocsBoug[i]->getPosy()]='4';
        }else matriceJeu[tabBlocsBoug[i]->getPosx()][tabBlocsBoug[i]->getPosy()]='3';

    }
    for(unsigned int i=0;i<tabBlocsCass.size();i++)
    {
        if(tabBlocsCass[i]->getEtat()==1)
        {
            matriceJeu[tabBlocsCass[i]->getPosx()][tabBlocsCass[i]->getPosy()]='C';
        }else matriceJeu[tabBlocsCass[i]->getPosx()][tabBlocsCass[i]->getPosy()]='.';

    }
    for(unsigned int i=0;i<tabBlocsTue.size();i++)
    {
            matriceJeu[tabBlocsTue[i]->getPosx()][tabBlocsTue[i]->getPosy()]='T';
    }

    matriceJeu[personnage.getPosx()][personnage.getPosy()]='S';
    char transit=matriceJeu[balle.getPosx()][balle.getPosy()];
    matriceJeu[balle.getPosx()][balle.getPosy()]='B';
    int x=20;
        pConsole->gotoLigCol(0,x);
        cout<<" _____________________________________________________________________________"<<endl<<endl;
        pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888  .d88888b.   .d88888b.  8888888b. Y88b   d88P    |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888 d88P\" \"Y88b d88P\" \"Y88b 888   Y88b Y88b d88P     |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888 888     888 888     888 888    888  Y88o88P      |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888 888     888 888     888 888   d88P   Y888P       |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888 888     888 888     888 8888888P\"     888        |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888 888     888 888     888 888           888        |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888 Y88b. .d88P Y88b. .d88P 888           888        |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888  \"Y88888P\"   \"Y88888P\"  888           888        |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
    for(int i=0; i<10; i++)
    {
        pConsole->gotoLigCol(11+i,x+10);
        for (int j=0; j<20; j++)
        {
            switch(matriceJeu[i][j])
            {pConsole->setColor(COLOR_GREEN);
                case '.':pConsole->setColor(COLOR_DEFAULT);break;
                case 'B':pConsole->setColor(Cballe);break;
                case '3':pConsole->setColor(COLOR_KAKI);break;
                case '4':pConsole->setColor(COLOR_GREEN);break;
                case 'S':pConsole->setColor(COLOR_BLUE);break;
                case 'T':pConsole->setColor(COLOR_RED);break;
                case '1':pConsole->setColor(COLOR_YELLOW);break;
                case 'C':pConsole->setColor(COLOR_BROWN);break;
            }
            cout << " "<< matriceJeu[i][j]<<" ";
        }
        cout << endl;
    }
    matriceJeu[balle.getPosx()][balle.getPosy()]=transit;
    pConsole->setColor(COLOR_WHITE);
    Console::deleteInstance();
}


void Niveaux::loadNiveau(int n)
{
    int a=0, b=0, c=0, d=0, e=0;
    int obj, blocsimmo, blocsboug, blocstue, blocscass;

    if (n_num==1)
    {
        ifstream fichier("Niveaux/niveau1.txt", ios::in);  // on ouvre le fichier en lecture
        if(fichier.is_open())  // si l'ouverture a r�ussi
        {
            // instructions
            for(int i=0; i<10; i++)
            {
                for(int j=0; j<=20; j++)
                {
                    matriceJeu[i][j]=fichier.get();
                }
            }
            fichier.close();  // on ferme le fichier
            //cout << "Bien Charge !!";

        }
        else  // sinon
        std::cerr << "Impossible d'ouvrir le fichier !" << endl;
        //std::cout << "Impossible d'ouvrir le fichier !" << endl;

        ifstream fichier2("Niveaux/objectifs1.txt", ios::in);
        {
            if(fichier2.is_open())
            {
                fichier2 >> obj >> blocsimmo >> blocsboug >> blocstue >> blocscass;  /*on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e */

                fichier2.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    if (n_num==2)
    {
        ifstream fichier("Niveaux/niveau2.txt", ios::in);  // on ouvre le fichier en lecture
        if(fichier.is_open())  // si l'ouverture a r�ussi
        {
            // instructions
            for(int i=0; i<10; i++)
            {
                for(int j=0; j<=20; j++)
                {
                    matriceJeu[i][j]=fichier.get();
                }
            }
            fichier.close();  // on ferme le fichier
            //cout << "Bien Charge !!";

        }
        else  // sinon
        std::cerr << "Impossible d'ouvrir le fichier !" << endl;
        //std::cout << "Impossible d'ouvrir le fichier !" << endl;

        ifstream fichier2("Niveaux/objectifs2.txt", ios::in);
        {
            if(fichier2.is_open())
            {
                fichier2 >> obj >> blocsimmo >> blocsboug >> blocstue >> blocscass;  /*on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e */

                fichier2.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    if (n_num==3)
    {
        ifstream fichier("Niveaux/niveau3.txt", ios::in);  // on ouvre le fichier en lecture
        if(fichier.is_open())  // si l'ouverture a r�ussi
        {
            // instructions
            for(int i=0; i<10; i++)
            {
                for(int j=0; j<=20; j++)
                {
                    matriceJeu[i][j]=fichier.get();
                }
            }
            fichier.close();  // on ferme le fichier
            //cout << "Bien Charge !!";

        }
        else  // sinon
        std::cerr << "Impossible d'ouvrir le fichier !" << endl;
        //std::cout << "Impossible d'ouvrir le fichier !" << endl;

        ifstream fichier2("Niveaux/objectifs3.txt", ios::in);
        {
            if(fichier2.is_open())
            {
                fichier2 >> obj >> blocsimmo >> blocsboug >> blocstue >> blocscass;  /*on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e */

                fichier2.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    /*cout << endl << "Nombres d'objectifs:" << obj;
    cout << endl << "Nombres de blocs immobiles:" << blocsimmo;
    cout << endl << "Nombres de blocs poussables:" << blocsboug;
    cout << endl << "Nombres de blocs qui tuent:" << blocstue;*/

    ///////////////////////////////////////////////////////////////////On cr�e des tableaux de classes contenant les diverses instances de classe

    tabObj.resize(obj);
    tabBlocsImmo.resize(blocsimmo);
    tabBlocsBoug.resize(blocsboug);
    tabBlocsTue.resize(blocstue);
    tabBlocsCass.resize(blocscass);


    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            switch (matriceJeu[i][j])
            {
                case '1':   tabObj[a] = new Objectifs(i, j, 1);
                            a++;
                            break;

                case '3':   tabBlocsImmo[b] = new BlocsImmo(i, j, 1, 0);
                              b++;
                              break;

                case '4':   tabBlocsBoug[c] = new BlocsBoug(i, j, 1, 0);
                            c++;
                            break;

                case '5':   tabBlocsTue[d] = new BlocsTue(i, j, 1, 1);
                            d++;
                            break;

                case '6':   tabBlocsCass[e] = new BlocsCass(i, j, 1, 2);
                            e++;
                            break;
            }
        }
    }


    /*cout << endl << "posx du deuxieme:" << tabObj[1]->getPosy() << endl;
    cout << endl << "posx du deuxieme:" << tabBlocsImmo[1]->getPosy() << endl;
    cout << endl << "posy du deuxieme:" << tabBlocsImmo[1]->getPosx() << endl;*/

    //On cr�er un tableau de classe avec les positions

}


void Niveaux::sauvegarder()
{
    int ois=0, immo=0, boug=0, tue=0, cass=0;
    int obj=0, blocsimmo=0, blocsboug=0, blocstue=0, blocscass=0;

    //////////////////////////////////////////////////////////////On cr�e 2 fichiers de sauvegardes dans le dossier source

    if (getNum()==1)
    {
        ifstream fichier("Niveaux/objectifs1.txt", ios::in);
        {
            if(fichier.is_open())
            {
                fichier >> ois >> immo >> boug >> tue >> cass;  //on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e

                fichier.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    if (getNum()==2)
    {
        ifstream fichier("Niveaux/objectifs2.txt", ios::in);
        {
            if(fichier.is_open())
            {
                fichier >> ois >> immo >> boug >> tue >> cass;  //on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e

                fichier.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    if (getNum()==3)
    {
        ifstream fichier("Niveaux/objectifs3.txt", ios::in);
        {
            if(fichier.is_open())
            {
                fichier >> ois >> immo >> boug >> tue >> cass;  //on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e

                fichier.close();
            }
            else    cerr << "Impossible d'ouvrir le fichier !" << endl;

        }
    }

    string nom;
    cout << "Sauvegarde en cours !" << endl <<"Votre nom (sans espace): " << endl << "=> ";
    cin >> nom;

    string objec = "Obj";
    string extension = ".txt";
    string tmp2 = nom + extension;
    string tmp = objec + nom;
    string tmp3 = tmp + extension;
    string chemin = "Saves/";
    string savename = chemin + tmp2;
    string saveobj = chemin + tmp3;

    ////////////////////////////////////////////////////////////
    for (int i=0; i<ois; i++)
    {
        if(tabObj[i]->getEtat()==1)
            obj++;
    }

    for (int i=0; i<immo; i++)
    {
        if(tabBlocsImmo[i]->getEtat()==1)
            blocsimmo++;
    }

    for (int i=0; i<boug; i++)
    {
        if(tabBlocsBoug[i]->getEtat()==1)
            blocsboug++;
    }

    for (int i=0; i<tue; i++)
    {
        if(tabBlocsTue[i]->getEtat()==1)
            blocstue++;
    }

    for (int i=0; i<cass; i++)
    {
        if(tabBlocsCass[i]->getEtat()==1)
            blocscass++;
    }

    ofstream fichier1(saveobj.c_str(), ios::out | ios::trunc);  // ouverture en �criture avec effacement du fichier ouvert
    {
        if(fichier1.is_open())
        {
            fichier1 << getNum() << endl;
            fichier1 << obj << endl;
            fichier1 << blocsimmo << endl;
            fichier1 << blocsboug << endl;
            fichier1 << blocstue << endl;
            fichier1 << blocscass << endl;
            fichier1 << personnage.getPosy() <<" " << personnage.getPosx() << endl;     //<------------- si public utiliser snoopy.s_posx
            fichier1 << personnage.getVie() << endl;
            fichier1 << balle.getPosy() <<" " <<balle.getPosx() << " " <<balle.getDy() << " " <<balle.getDx() << endl;
            fichier1 << contrat.getTimer() << endl;
            fichier1 << contrat.getScoreTotal() << endl;


            fichier1.close();
        }
        else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }

    ///////////////////////////////////////////////On cr�e une matrice remplie de 0 et on actualise avec les donn�es que l'on a afin de la sauvegarder dans un fichier txt

    vector< vector<char> > savemat;
    savemat.resize(10, vector< char >(20,NULL));
    for (int i=0; i<10; i++)
    {
        for(int j=0; j<20; j++)
        {
            savemat[i][j]='0';
        }
    }

    for (int i=0; i<ois; i++)
    {
        if(tabObj[i]->getEtat()==1)
            savemat[tabObj[i]->getPosx()][tabObj[i]->getPosy()] = '1';
    }

    for (int i=0; i<immo; i++)
    {
        if(tabBlocsImmo[i]->getEtat()==1)
            savemat[tabBlocsImmo[i]->getPosx()][tabBlocsImmo[i]->getPosy()] = '3';
    }

    for (int i=0; i<boug; i++)
    {
        if(tabBlocsBoug[i]->getEtat()==1)
            savemat[tabBlocsBoug[i]->getPosx()][tabBlocsBoug[i]->getPosy()] = '4';
    }
    for (int i=0; i<boug; i++)
    {
        if(tabBlocsBoug[i]->getEtat()==0)
            savemat[tabBlocsBoug[i]->getPosx()][tabBlocsBoug[i]->getPosy()] = '4';
    }


    for (int i=0; i<tue; i++)
    {
        if(tabBlocsTue[i]->getEtat()==1)
            savemat[tabBlocsTue[i]->getPosx()][tabBlocsTue[i]->getPosy()] = '5';
    }

    for (int i=0; i<cass; i++)
    {
        if(tabBlocsCass[i]->getEtat()==1)
            savemat[tabBlocsCass[i]->getPosx()][tabBlocsCass[i]->getPosy()] = '6';
    }

    ofstream fichier2(savename.c_str(), ios::out | ios::trunc);  // ouverture en �criture avec effacement du fichier ouvert
    {
        if(fichier2.is_open())
        {
            for(int i=0; i<10; i++)
            {
                for (int j=0; j<20; j++)
                {
                    fichier2 << savemat[i][j];
                }
                fichier2 << endl;
            }

            fichier2.close();
        }
        else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }

}

void Niveaux::chargerSave()
{
    string nom;
    cout << "Veuillez saisir le nom de votre sauvegarde sans espace: ";
    cin >> nom;

    string objec = "Obj";
    string extension = ".txt";
    string tmp2 = nom + extension;
    string tmp = objec + nom;
    string tmp3 = tmp + extension;
    string chemin = "Saves/";
    string lastname = chemin + tmp2;
    string lastobj = chemin + tmp3;
    string acces;
    string niveau;
    string tmp4 = "Niveaux/objectifs";



    /////////////////////////////////////////////On recharge les tableaux et les sp�cificit�s de chaque instance de classe
    int obj=0, blocsimmo=0, blocsboug=0, blocstue=0, blocscass=0;
    int objo=0, blocsimmoo=0, blocsbougo=0, blocstueo=0, blocscasso=0;
    int snoopx, snoopy, snoopvie, ballex, balley, balledx, balledy, timer, score;

    ifstream fichier(lastobj.c_str(), ios::in);
    {
        if(fichier.is_open())
        {
            fichier >> niveau;
            fichier >> obj >> blocsimmo >> blocsboug >> blocstue >> blocscass;  //on lit jusqu'� l'espace et on stocke ce qui est lu dans la variable indiqu�e
            fichier >> snoopx >> snoopy >> snoopvie;
            fichier >> ballex >> balley >> balledx >> balledy;
            fichier >> timer >> score;

            fichier.close();
        }
        else    cerr << "Impossible d'ouvrir le fichier !" << endl;

    }

    string tmp5 = tmp4 + niveau;
    acces = tmp5 + extension;

    ifstream fichier8(acces.c_str(), ios::in);
    {
        if(fichier8.is_open())
        {
            fichier8 >> objo;
            fichier8 >> blocsimmoo;
            fichier8 >> blocsbougo;
            fichier8 >> blocstueo;
            fichier8 >> blocscasso;

            fichier8.close();
        }
        //else    cerr << "Impossible d'ouvrir le fichier !" << endl;

    }

    for(int i=0; i<objo; i++)
    {
        tabObj[i]=NULL;
    }
    for(int i=0; i<blocsimmoo; i++)
    {
        tabBlocsImmo[i]=NULL;
    }
    for(int i=0; i<blocsbougo; i++)
    {
        tabBlocsBoug[i]=NULL;
    }
    for(int i=0; i<blocstueo; i++)
    {
        tabBlocsTue[i]=NULL;
    }
    for(int i=0; i<blocscasso; i++)
    {
        tabBlocsCass[i]=NULL;
    }




    tabObj.resize(obj);
    tabBlocsImmo.resize(blocsimmo);
    tabBlocsBoug.resize(blocsboug);
    tabBlocsTue.resize(blocstue);
    tabBlocsCass.resize(blocscass);

    personnage.setPosx(snoopy);
    personnage.setPosy(snoopx);
    personnage.setVie(snoopvie);

    // De meme pour la classe contrat
    contrat.setTimer(timer);
    contrat.setScore(score);

    //De meme pour la balle instance de ennemis
    balle.setPosx(ballex);
    balle.setPosy(balley);
    balle.setDx(balledx);
    balle.setDy(balledy);


    ///////////////////////////////////////////////On charge la matrice du fichier de sauvegarde en cr�ant une matrice et lisant le fichier qui remplit la matrice
    //////////////////////////////////////////////Probleme lors du chargement de la matrice
    ifstream fichier2(lastname.c_str(), ios::in);
    {
        if(fichier2.is_open())
        {
            int a=0, b=0, c=0, d=0, e=0;

            for (int i=0; i<=10; i++)
            {
                for (int j=0; j<20; j++)
                {
                    fichier2 >> matriceJeu[i][j];
                    switch (fichier.get())
                    {
                        case '1' :   tabObj[a]->setPosx(i);
                                     tabObj[a]->setPosy(j);
                                     tabObj[a]->setEtat(1);
                                     //etat est a 1 par d�faut mais v�rifier
                                     a++;
                                     break;

                        case '3' :  tabBlocsImmo[b]->setPosx(i);
                                    tabBlocsImmo[b]->setPosy(j);
                                    tabBlocsImmo[b]->setEtat(1);
                                    b++;
                                    break;

                        case '4' :  tabBlocsBoug[c]->setPosx(i);
                                    tabBlocsBoug[c]->setPosy(j);
                                    tabBlocsBoug[c]->setEtat(1);
                                    c++;
                                    break;

                        case '5' :  tabBlocsTue[d]->setPosx(i);
                                    tabBlocsTue[d]->setPosy(j);
                                    tabBlocsTue[d]->setEtat(1);            //<-------- en public tabBlocTue[b]->b_posy = j;
                                    d++;
                                    break;

                        case '6' :  tabBlocsCass[e]->setPosx(i);
                                    tabBlocsCass[e]->setPosy(j);
                                    tabBlocsCass[e]->setEtat(1);            //<-------- en public tabBlocTue[b]->b_posy = j;
                                    e++;
                                    break;
                }
            }
        }


        fichier.close();  // on ferme le fichier
        cout << "Bien Charge !!" << endl << endl;

        }
        else  // sinon
            std::cerr << "Impossible d'ouvrir le fichier !" << endl;
    }

}

bool Niveaux::deterCollisionx()
{
    bool change=false;
    for(unsigned int i=0;i<tabBlocsImmo.size();i++)
    {
        if((balle.getPosx() + balle.getDx())== tabBlocsImmo[i]->getPosx() && (balle.getPosy() + balle.getDy()) == tabBlocsImmo[i]->getPosy())
        {
            change=true;

        }
    }
    for(unsigned int i=0;i<tabBlocsBoug.size();i++)
    {
        if(balle.getPosx()+balle.getDx()==tabBlocsBoug[i]->getPosx() && (balle.getPosy() + balle.getDy()) == tabBlocsBoug[i]->getPosy())
        {
            change=true;

        }
    }
    for(unsigned int i=0;i<tabBlocsTue.size();i++)
    {
        if(balle.getPosx()+balle.getDx()==tabBlocsTue[i]->getPosx() && (balle.getPosy() + balle.getDy()) == tabBlocsTue[i]->getPosy())
        {
            change=true;
        }
    }
    for(unsigned int i=0;i<tabBlocsCass.size();i++)
    {
        if(balle.getPosx()+balle.getDx()==tabBlocsCass[i]->getPosx() && (balle.getPosy() + balle.getDy()) == tabBlocsCass[i]->getPosy())
        {
            change=true;
        }
    }
    return change;
}

bool Niveaux::determinerColision()
{
    bool collision=false;
    if(balle.getPosx()==personnage.getPosx() && balle.getPosy()==personnage.getPosy())collision=true;
    return collision;
}

bool Niveaux::verifObjectif()
{
    bool win=false;
    unsigned int objAccomplis=0;
    for(unsigned int i=0;i<tabObj.size();i++)
    {
        if(tabObj[i]->getEtat()==0) objAccomplis++;
    }
        if(tabObj.size()==objAccomplis) win=true;

    return win;

}
