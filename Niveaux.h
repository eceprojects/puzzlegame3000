#ifndef NIVEAUX_H_INCLUDED
#define NIVEAUX_H_INCLUDED

#include <string>
#include <vector>
#include <fstream>
#include "BlocsImmo.h"
#include "BlocsBoug.h"
#include "BlocsTue.h"
#include "Snoopy.h"
#include "Ennemis.h"
#include "Contrat.h"
#include "Objectifs.h"
#include "BlocsCass.h"
#include "console.h"

class Niveaux
{

private:

    int n_num; //le num�ro du niveau
    int n_numMax; //le num max

public:
    Console *pConsole;
    Snoopy personnage;
    Ennemis balle;
    Contrat contrat;
    std::vector<std::vector<char> >matriceJeu;

    std::vector<Objectifs* >tabObj;
    std::vector<BlocsImmo* >tabBlocsImmo;
    std::vector<BlocsBoug* >tabBlocsBoug;
    std::vector<BlocsTue* >tabBlocsTue;
    std::vector<BlocsCass* >tabBlocsCass;

    //Constructeurs et destructeur
    Niveaux();
    Niveaux(int _num, int _numMax);
    ~Niveaux();

    //Methodes
    /*
        Tous les sp li�s � la classe
    */
    void setColor(int front, int back);
    void deplacerPersonnage(int key);
    void setNiveau();
    void afficherNiveau();
    void Niveau1();
    void sauvegarder();
    void loadNiveau(int n_num);
    void chargerSave();
    bool determinerColision();
    bool verifObjectif();
    bool deterCollisionx();
    bool deterCollisiony();
    void afficherScore(time_t temps1, int& scoretmp, int cmp,int nbvie);

//  void jouerNiveau()

    //Getter et setter
    int getNum() const;
    int getNumMax() const;

    void setNum(int numlvl);
    void setNumMax(int numlvlmax);

};

#endif // NIVEAUX_H INCLUDED
