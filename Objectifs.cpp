#include <string>
#include "Objectifs.h"
#include <iostream>

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
Objectifs::Objectifs() : o_posx(0), o_posy(0), o_etat(1)
{
}

Objectifs::Objectifs(int x, int y, int etat) : o_posx(x), o_posy(y), o_etat(etat)
{
}

Objectifs::~Objectifs()
{
}

////////////////////////////////////////Getter et Setter

int Objectifs::getPosx() const
{
    return o_posx;
}

int Objectifs::getPosy() const
{
    return o_posy;
}

int Objectifs::getEtat() const
{
    return o_etat;
}

void Objectifs::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        o_posx = positionx;
}

void Objectifs::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        o_posy = positiony;
}

void Objectifs::setEtat(int eta)
{
    if (eta==0 || eta==1)
        o_etat = eta;
}


//////////////////////////////////////////////
void Objectifs::afficher()
{
    //std::cout << "O";
}
