#ifndef OBJECTIFS_H_INCLUDED
#define OBJECTIFS_H_INCLUDED

#include <string>


class Objectifs
{

private:
    int o_posx, o_posy; //la position
    int o_etat; //recup�r� ou pas encore

public:
    //Constructeurs et destructeur
    Objectifs();
    Objectifs(int x, int y, int etat);
    ~Objectifs();

    //Methodes
    /*
        Tous les sp li�s � la classe
    */
    void afficher();

    //Getter et setter
    int getPosx() const;
    int getPosy() const;
    int getEtat() const;

    void setPosx(int positionx);
    void setPosy(int positiony);
    void setEtat(int eta);

};

#endif // OBJECTIFS_H INCLUDED
