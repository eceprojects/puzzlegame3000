#include <string>
#include<iostream>
#include "Snoopy.h"

using namespace std;

// /!\On y met les constructeurs et les m�thodes dont les attributs et les prototypes

//////////////////////////////////////////Constructeurs et destructeurs
Snoopy::Snoopy() : s_posx(4), s_posy(9), s_dx(0), s_dy(0), s_etat(1), s_vie(3)
{
}

Snoopy::Snoopy(int _x, int _y, int _depx, int _depy, int _etat, int _vie) : s_posx(_x), s_posy(_y), s_dx(_depx), s_dy(_depy), s_etat(_etat), s_vie(_vie)
{
}

Snoopy::~Snoopy()
{
}
///Methodes


////////////////////////////////////////Getter et Setter
int Snoopy::getPosx() const
{
    return s_posx;
}

int Snoopy::getPosy() const
{
    return s_posy;
}

int Snoopy::getDx() const
{
    return s_dx;
}

int Snoopy::getDy() const
{
    return s_dy;
}

int Snoopy::getEtat() const
{
    return s_etat;
}

int Snoopy::getVie() const
{
    return s_vie;
}

void Snoopy::setPosx(int positionx)
{
    if (positionx>=0 && positionx<10)
        s_posx = positionx;
}

void Snoopy::setPosy(int positiony)
{
    if (positiony>=0 && positiony<20)
        s_posy = positiony;
}

void Snoopy::setDx(int deplacementx)
{
    if (deplacementx<-1 && deplacementx>1)
        s_dx = deplacementx;
}

void Snoopy::setDy(int deplacementy)
{
    if (deplacementy<-1 && deplacementy>1)
        s_dy = deplacementy;
}

void Snoopy::setEtat(int eta)
{
    if (eta==0 || eta==1)
        s_etat = eta;
}

void Snoopy::setVie(int nbvie)
{
    if (nbvie>=0 && nbvie<=3)
        s_vie = nbvie;
}
