#ifndef SNOOPY_H_INCLUDED
#define SNOOPY_H_INCLUDED

#include <string>


class Snoopy
{

private:
    int s_posx, s_posy; //la position
    int s_dx, s_dy; //le mouvement
    int s_etat; //l'etat mort ou vivant 1 = vivant et 0 = mort
    int s_vie; //le nb de vie
    char touche;

public:
    //Constructeurs et destructeur
    Snoopy();
    Snoopy(int _x, int _y, int _depx, int _depy, int _etat, int _vie);
    ~Snoopy();

    //Methodes
    /*
        Tous les sp li�s � la classe
    */

    //Getter et setter
    int getPosx() const;
    int getPosy() const;
    int getDx() const;
    int getDy() const;
    int getEtat() const;
    int getVie() const;

    void setPosx(int positionx);
    void setPosy(int positiony);
    void setDx(int deplacementx);
    void setDy(int deplacementy);
    void setEtat(int eta);
    void setVie(int nbvie);

};

#endif // SNOOPY_H INCLUDED

