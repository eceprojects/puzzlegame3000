#include "Timer.h"

Timer::Timer(short int sec)
{
    m_reference = time(0);

    if (sec > 0)
    {
        m_timeMax = sec;
    }
    else
    {
        m_timeMax = 0;
        std::cout << "Error, timer is set to a value <= 0" << std::endl;
    }

}

Timer::~Timer()
{
    // Destructor
}

bool Timer::resetTimer(short int sec)
{
    m_reference = time(0);

    if (sec > 0)
    {
        m_timeMax = sec;
        return 0;
    }
    else
    {
        m_timeMax = 0;
        std::cout << "Error, timer is set to a value <= 0" << std::endl;
        return 1;
    }

}


short int Timer::getTime() // Get time left
{
    short int result = m_timeMax - (short int)(difftime(time(0),m_reference));
    if(result > 0)
        return result;
    else
        return 0;
}

bool Timer::alterate(short int sec)
{
    m_timeMax+=(short int)sec;
    return 0;
}
