#ifndef TIMER_H
#define TIMER_H
#include <time.h>
#include <iostream>

class Timer
{
    public:
        Timer(short int sec); // Starts a timer with 'sec' seconds left
        ~Timer();

        short int getTime(); // Get time left
        bool alterate(short int sec); // Adds or subtract time in seconds
        bool resetTimer(short int sec); // Reset with 'sec' seconds left

    private:
        short int m_timeMax;
        time_t m_reference;
};

#endif // TIMER_H
