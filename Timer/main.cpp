#include <iostream>
#include <time.h>
#include "Timer.h"

int main()
{
    std::string test;

    Timer a(20);
    std::cin >> test;
    std::cout << a.getTime() << std::endl;
    return 0;
}
