#include "console.h"
#include <conio.h>
#include <windows.h>

Console* Console::m_instance = NULL;

Console::Console()
{
    m_instance = NULL;
}

Console::~Console()
{

}

void Console::deleteInstance()
{
    delete m_instance;
    m_instance = NULL;
}

Console* Console::getInstance()
{
    if (!Console::m_instance)
    {
        m_instance = new Console();
    }

    return m_instance;
}

void Console::gotoLigCol(int lig, int col)
{
    COORD mycoord;
    mycoord.X = col;
    mycoord.Y = lig;
    SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), mycoord );
}

int Console::getInputKey()
{
    return getch();
}

bool Console::isKeyboardPressed()
{
    return kbhit();
}

/*
0: noir
1: bleu fonc�
2: vert
3: bleu-gris
4: marron
5: pourpre
6: kaki
7: gris clair
8: gris
9: bleu
10: vert fluo
11: turquoise
12: rouge
13: rose fluo
14: jaune fluo
15: blanc
*/
void Console::_setColor(int back, int front)
{
    HANDLE H=GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(H,front*16+back);
}

void Console::setColor(Color col)
{
    switch (col)
    {
        case COLOR_JAUNE:
            this->_setColor(14,0);
            break;
        case COLOR_WHITE:
            this->_setColor(15,0);
            break;
        case COLOR_BLACK:
            this->_setColor(0, 0);
            break;
            case COLOR_ROUGE:
            this->_setColor(12,0);
            break;
        case COLOR_RED:
            this->_setColor(0, 12);
            break;
        case Cballe:
            this->_setColor(12,7);
            break;
        case COLOR_GREEN:
            this->_setColor(0, 10);
            break;
        case COLOR_BLUE:
            this->_setColor(0, 9);
            break;
        case COLOR_YELLOW:
            this->_setColor(0, 14);
            break;
        case COLOR_PURPLE:
            this->_setColor(0, 5);
            break;
        case COLOR_BROWN:
            this->_setColor(0, 4);
            break;
        case COLOR_KAKI:
            this->_setColor(0, 6);
            break;

        default:
            this->_setColor(7, 7);
    }
}
