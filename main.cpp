// This project is available publicly at : https://bitbucket.org/eceprojects/puzzlegame3000/commits/all
// Recommended width for console is 115 char

#include <iostream>
#include <vector>
#include <fstream>
#include "Snoopy.h"
#include "Ennemis.h"
#include "Niveaux.h"
#include "Objectifs.h"
#include "console.h"
#include "BlocsImmo.h"
#include "BlocsBoug.h"
#include "BlocsTue.h"
#include <stdlib.h>
#include <time.h>
#include <string>


using namespace std;

void pause()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int a=0;
    do
    {
        pConsole->gotoLigCol(15, 53);
        cout<<"[PAUSE]";
        a = pConsole->getInputKey();
    }while(a!='p' && a!= 27);
}

void gameover()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    system("cls");
            pConsole->gotoLigCol(7,10);
            cout << "  _______      ___      .___  ___.  _______      ______   ____    ____  _______ .______      ";
            pConsole->gotoLigCol(8,10);
            cout << " /  _____|    /   \\     |   \\/   | |   ____|    /  __  \\  \\   \\  /   / |   ____||   _  \\     ";
            pConsole->gotoLigCol(9,10);
            cout << "|  |  __     /  ^  \\    |  \\  /  | |  |__      |  |  |  |  \\   \\/   /  |  |__   |  |_)  |    ";
            pConsole->gotoLigCol(10,10);
            cout << "|  | |_ |   /  /_\\  \\   |  |\\/|  | |   __|     |  |  |  |   \\      /   |   __|  |      /     ";
            pConsole->gotoLigCol(11,10);
            cout << "|  |__| |  /  _____  \\  |  |  |  | |  |____    |  `--'  |    \\    /    |  |____ |  |\  \----.";
            pConsole->gotoLigCol(12,10);
            cout << " \\______| /__/     \\__\\ |__|  |__| |_______|    \\______/      \\__/     |_______|| _| `.___|";
            pConsole->gotoLigCol(20,0);
            system("pause");
}

int jouerPartie(Niveaux current, int& scoretmp)
{
    int jouer = 1,score=0;
    time_t temps1,temps2;
    temps1 = time(NULL);
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int clef;
    int temps;
    bool changex,changey;
    while(jouer==1)
    {
        changex=false;
        if (pConsole->isKeyboardPressed())
        {
        // R�cup�re le code ASCII de la touche
        clef = pConsole->getInputKey();
        current.deplacerPersonnage(clef);
        //Pause
        if(clef=='p' || clef=='P')
        {
            temps=difftime(temps2,temps1);
            pause();
            temps1=difftime(time(NULL),temps);
        }
        if (clef=='m' || clef=='M')
        {
            pConsole->gotoLigCol(27,0);
            current.contrat.setScoreTotal(scoretmp);
            current.sauvegarder();
            jouer = 0;
        }

        pConsole->gotoLigCol(0, 0);
        current.afficherNiveau();


        }
        changex=current.deterCollisionx();

        int cmp = 0;
        for (int i=0; i<4; i++)
        {
            if(current.tabObj[i]->getEtat()==1)
                cmp++;
        }
        temps2=time(NULL);

        score = current.contrat.score(); //calcul le score total

        if (current.personnage.getVie()==0 || difftime(temps2,temps1)>60 || clef==27)
        {
            gameover();
            jouer = 0;
        }
        if(current.verifObjectif()==true)
        {
            scoretmp = score + scoretmp;
            cout << "scoretmp : " << scoretmp;
            return 1;
        }
        if(current.balle.deplacerEnnemi(changex)==true)
        {
            pConsole->gotoLigCol(0, 0);
            current.afficherNiveau();
            if(current.determinerColision()==true)current.personnage.setVie(current.personnage.getVie()-1);
            current.afficherScore(temps1, scoretmp, cmp,current.personnage.getVie());
        }

        ///////////////////////////////////////////////On affiche sur le cote les infos relatives a la partie

    }


        // Lib�re la m�moire du pointeur !
        Console::deleteInstance();
        return jouer;
}
void debuterPartie(int lvl, int& scoretmp)
{
    if(lvl<4)
    {
        Niveaux niveau;
        niveau.setNum(lvl);
        niveau.loadNiveau(niveau.getNum());
        niveau.contrat.setTimerMax(60);
        int score = 0;
        int jouer=jouerPartie(niveau, scoretmp);
        niveau.contrat.setScoreTotal(scoretmp);
        if(jouer==1)
        {
            score += scoretmp;
            niveau.contrat.setScore(scoretmp);
            lvl++;
            debuterPartie(lvl, score);
        }
    }


}

void afficherRegles()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int clef;
    int jouer = 1;
    pConsole->gotoLigCol(24, 5);
    cout << "Vous etes Snoopy identifiable par son S et votre mission est de sauver les 4 oiseaux au coin du niveau note O.";
    pConsole->gotoLigCol(25, 18);
    cout << "Pour cela, vous allez devoir resoudre un puzzle compose de differents blocs.";
    pConsole->gotoLigCol(26, 11);
    cout << "Ils peuvent aussi bien etre immobiles comme deplacables ou encore cassables mais aussi fatals.";
    pConsole->gotoLigCol(27, 19);
    cout << "Alors deplacez vous a l'aide des touches ZQSD et cassez les blocs avec A.";
    pConsole->gotoLigCol(28, 15);
    cout << "Mais faites aussi attention a la balle meurtiere Vous n'avez droit que 3 essais!";
    pConsole->gotoLigCol(29, 43);
    cout << "Bonne chance aventurier :)";
    pConsole->gotoLigCol(30, 40);
    cout << endl << endl << "Appuyer sur ESC pour revenir au menu";
    clef = pConsole->getInputKey();
    while (clef!=27)
    {
        system("cls");
        pConsole->gotoLigCol(13, 40);
        cout << "Appuyer sur ESC pour revenir au menu";
        clef = pConsole->getInputKey();
    }
    system("cls");
    jouer = 0;
    Console::deleteInstance();
}

void afficherCredits()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    int clef;
    int jouer = 1;
    pConsole->gotoLigCol(31, 25);
    cout << "LA REVANCHE DE SNOOPY, un jeu developpe avec la participation de :";
    pConsole->gotoLigCol(32, 49);
    cout << "VICTOR MONOMAKHOFF";
    pConsole->gotoLigCol(33, 51);
    cout << "GUILLAUME ROWE";
    pConsole->gotoLigCol(34, 50);
    cout << "RAPHAEL CASIMIR";
    pConsole->gotoLigCol(36, 0);
    cout << "Appuyer sur ESC pour revenir au menu";
    clef = pConsole->getInputKey();
    while (clef!=27)
    {
        system("cls");
        pConsole->gotoLigCol(13, 40);
        cout << "Appuyer sur ESC pour revenir au menu";
        clef = pConsole->getInputKey();
    }
    jouer = 0;
    system("cls");
    Console::deleteInstance();
}


int nouvellePartie()
{
    int lvl=1;
    return lvl;
}
void chargerPartie()
{
    int score = 0;
    int& scoretmp = score;
    char choix;
            bool jouer = true;

            while(jouer != false)
            {
                do{
                        fflush(stdin);
                        system("cls");
                        cout << "Voulez-vous charger une ancienne sauvegarde ? (O/N): ";
                        cin >> choix;
                }while (choix!='O' && choix!='o' && choix!='N' && choix!='n');
                system("cls");
                if(choix=='O' || choix=='o')
                {
                    Niveaux current;
                    current.loadNiveau(1);
                    current.chargerSave();
                    for (int i=0; i<10; i++)
                    {
                        for (int j=0; j<20; j++)
                        {
                            switch(current.matriceJeu[i][j])
                            {
                                case '0':   cout << ".";
                                            break;
                                case '1':   cout << "O";
                                            break;
                                case '3':   cout << "I";
                                            break;
                                case '4':   cout << "P";
                                            break;
                                case '5':   cout << "T";
                                            break;
                                case '6':   cout << "C";
                                            break;
                            }
                        }
                        cout << endl;
                    }
                    cout << endl;
                    system("pause");
                    break;

                    /*
                        continuer la boucle de jeu apres l'affichage
                    */
                }
                if(choix=='N' || choix=='n')
                {
                    char choix2;

                    do{
                            system("cls");
                            cout << "Avez-vous un mot de passe pour acceder a un niveau particulier ? (O/N): ";
                            cin >> choix2;
                    }while (choix2!='O' && choix2!='o' && choix2!='N' && choix2!='n');

                    if (choix2=='O' || choix2=='o')
                    {
                        string mdpsaisie;
                        string mdp1 = "lvl1";
                        string mdp2 = "lvl2";
                        string mdp3 = "lvl3";
                        cout << endl << "Veuillez saisir le mot de passe : ";
                        cin >> mdpsaisie;
                        system("cls");
                        if (mdpsaisie==mdp1) ////////////////////////On charge le niveau 1
                        {
                            debuterPartie(1, scoretmp);
                            break;
                        }
                        if (mdpsaisie==mdp2) ////////////////////////On charge le niveau 2
                        {
                            debuterPartie(2, scoretmp);
                            break;
                        }
                        if (mdpsaisie==mdp3) /////////////////////////On charge le niveau 3
                        {
                            debuterPartie(3, scoretmp);
                            break;
                        }
                        ////////////////////////////////RETOUR MENU MAUVAIS MDP
                        if (mdpsaisie!=mdp1 && mdpsaisie!=mdp2 && mdpsaisie!=mdp3)
                        {
                            cout << endl << "Le mot de passe saisie n'est pas valide vous allez etre redirige vers le menu." << endl;
                            system("pause");
                            jouer = false;
                        }
                    }
                    else
                        jouer = false;
                }


            }
}


void afficherSnoopyMenu(int cas)
 {
    int x=20;
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    if(cas%10000==0)
    {
        cas=cas/10000;
        pConsole->gotoLigCol(12, 50);
            cout << "1) Nouvelle partie "<<endl;
            pConsole->gotoLigCol(13, 50);
            cout<<"2) Charger "<<endl;
            pConsole->gotoLigCol(14, 50);
            cout<<"3) Regles "<<endl;
            pConsole->gotoLigCol(15, 50);
            cout<<"4) Credits "<<endl;
            pConsole->gotoLigCol(16, 50);
            cout<<"5) Quitter";

            pConsole->gotoLigCol(18, 40);
            cout<<"           .o."<<endl;
            pConsole->gotoLigCol(19, 40);
            cout<<"          |  |    _   ,"<<endl;
            pConsole->gotoLigCol(20, 40);
            cout<<"        .',  L.-'` `\ ||"<<endl;
            pConsole->gotoLigCol(21, 40);
            cout<<"      __\\___,|__--,__`_|__"<<endl;
            pConsole->gotoLigCol(22, 40);
            cout<<"     |    %     `=`       |"<<endl;
            pConsole->gotoLigCol(23, 40);
            cout<<"     | ___%_______________|"<<endl;
            pConsole->gotoLigCol(24, 40);
            cout<<"     |    `               |"<<endl;
            pConsole->gotoLigCol(25, 40);
            cout<<"     | -------------------|"<<endl;
            pConsole->gotoLigCol(26, 40);
            cout<<"     |____________________|"<<endl;
            pConsole->gotoLigCol(27, 40);
            cout<<"       |~~~~~~~~~~~~~~~~|"<<endl;
            pConsole->gotoLigCol(28, 40);
            cout<<"       | ---------------|  ,"<<endl;
            pConsole->gotoLigCol(29, 40);
            cout<<"   \\|  | _______________| / /"<<endl;
            pConsole->gotoLigCol(30, 40);
            cout<<"\. \\,\\\|, .   .   /,  / |///, /"<<endl;
    }
    switch(cas)
    {
        case 1:
               pConsole->gotoLigCol(0,x);
               cout<<" _____________________________________________________________________________"<<endl<<endl;
               pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.                                                               |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b                                                              |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.                                                                   |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.                                                                |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b.                                                              |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888                                                              |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P                                                              |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"                                                               |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
               break;
        case 2:
               pConsole->gotoLigCol(0,x);
               cout<<" _____________________________________________________________________________"<<endl<<endl;
               pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888                                                  |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888                                                  |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888                                                  |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888                                                  |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888                                                  |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888                                                  |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888                                                  |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888                                                  |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
               break;
        case 3:
               pConsole->gotoLigCol(0,x);
               cout<<" _____________________________________________________________________________"<<endl<<endl;
               pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888  .d88888b.                                       |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888 d88P\" \"Y88b                                      |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888 888     888                                      |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888 888     888                                      |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888 888     888                                      |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888 888     888                                      |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888 Y88b. .d88P                                      |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888  \"Y88888P\"                                       |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
               break;
        case 4:
               pConsole->gotoLigCol(0,x);
               cout<<" _____________________________________________________________________________"<<endl<<endl;
               pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888  .d88888b.   .d88888b.                           |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888 d88P\" \"Y88b d88P\" \"Y88b                          |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888 888     888 888     888                          |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888 888     888 888     888                          |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888 888     888 888     888                          |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888 888     888 888     888                          |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888 Y88b. .d88P Y88b. .d88P                          |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888  \"Y88888P\"   \"Y88888P\"                           |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
               break;
        case 5:pConsole->gotoLigCol(0,x);
        cout<<" _____________________________________________________________________________"<<endl<<endl;
                pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888  .d88888b.   .d88888b.  8888888b.                |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888 d88P\" \"Y88b d88P\" \"Y88b 888   Y888               |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888 888     888 888     888 888    888               |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888 888     888 888     888 888   d88P               |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888 888     888 888     888 8888888P\"                |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888 888     888 888     888 888                      |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888 Y88b. .d88P Y88b. .d88P 888                      |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888  \"Y88888P\"   \"Y88888P\"  888                      |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;
               break;
        case 6:pConsole->gotoLigCol(0,x);
        cout<<" _____________________________________________________________________________"<<endl<<endl;
        pConsole->gotoLigCol(1,x);
               cout<<"|      .d8888b.  888b    888  .d88888b.   .d88888b.  8888888b. Y88b   d88P    |"<<endl;
               pConsole->gotoLigCol(2,x);
               cout<<"|     d88P  Y88b 8888b   888 d88P\" \"Y88b d88P\" \"Y88b 888   Y88b Y88b d88P     |"<<endl;
               pConsole->gotoLigCol(3,x);
               cout<<"|     Y88b.      88888b  888 888     888 888     888 888    888  Y88o88P      |"<<endl;
               pConsole->gotoLigCol(4,x);
               cout<<"|      \"Y888b.   888Y88b 888 888     888 888     888 888   d88P   Y888P       |"<<endl;
               pConsole->gotoLigCol(5,x);
               cout<<"|         \"Y88b. 888 Y88b888 888     888 888     888 8888888P\"     888        |"<<endl;
               pConsole->gotoLigCol(6,x);
               cout<<"|           \"888 888  Y88888 888     888 888     888 888           888        |"<<endl;
               pConsole->gotoLigCol(7,x);
               cout<<"|     Y88b  d88P 888   Y8888 Y88b. .d88P Y88b. .d88P 888           888        |"<<endl;
               pConsole->gotoLigCol(8,x);
               cout<<"|      \"Y8888P\"  888    Y888  \"Y88888P\"   \"Y88888P\"  888           888        |"<<endl;
               pConsole->gotoLigCol(9,x);
               cout<<"|_____________________________________________________________________________|"<<endl;

               break;
    }


}

void menu()
{
    Console* pConsole = NULL;
    pConsole = Console::getInstance();
    bool jouer = true;
    int score = 0;
    int i=1;
    int& scoretmp = score;
    while (jouer != false)
    {
        char a='a';

                afficherSnoopyMenu(i);
                i++;
                if(i==69999)i=0;

        if (pConsole->isKeyboardPressed())
        {
            a = pConsole->getInputKey();
        switch(a)
        {
        case '1':   system("cls");
                    debuterPartie(nouvellePartie(), scoretmp);
                    system("cls");
                    break;

        case '2':   chargerPartie();
                    system("cls");
                    break;

        case '3':   afficherRegles();
                    break;

        case '4':   afficherCredits();
                    break;

        case '5':   jouer = false;
                    break;
        }
        }
    }

}

int main()
{
    menu();
    return 0;
}
